---
title: "О подкасте"
date: 2020-08-24T22:40:07+03:00
tags: ["info"]
draft: false
---

Мы маленький и уютный подкаст про электронику и программирование!
Спасибо что нас слушаете, а если не слушаете, то скорее начинайте, вам точно понравится!

С наилучшими пожеланиями *[@likipiki](https://t.me/likipiki)*
