---
title: "Выпуск 3: Недооценненые 32 бита"
date: 2020-07-12T22:19:15+03:00
tags: ["podcast"]
draft: false
---

Обсудили STM32 микроконтроллеры и все что с ними связано. Как их программировать, какие библиотеки для них существуют. Плата для быстрого старта BLUEPILL

{{< episode "32-egk53h/a-a2m776m" >}}

