---
title: "Выпуск 5: Одноплатный компьютер"
date: 2020-10-24T9:08:14+03:00
tags: ["podcast"]
featured: false
draft: false
---

В этом выпуске мы обсудили одноплатные компьютеры такие как [Raspberry Pi](https://www.raspberrypi.org) и его аналоги. Что можно сделать используя их?
А так же поговорили о том, что такое [Flipper Zero и One](https://flipperzero.one) и зачем они нужны.

{{< episode "ep-elgr00/a-a3kt6ig" >}}
