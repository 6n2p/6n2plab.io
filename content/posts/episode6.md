---
title: "Выпуск 6: Пиратские тракторы"
date: 2021-03-02T17:30:14+03:00
tags: ["podcast"]
featured: true
draft: false
---
Ура, вышла новинка [Raspberry PI PICO](https://www.raspberrypi.org/products/raspberry-pi-pico/). Тракторы и [пиратский софт](https://habr.com/ru/company/itsumma/blog/542138/), хороша ли компьютеризация в сельском хозяйстве? Как написать [веб сайт для микроконтроллера](https://habr.com/ru/company/embox/blog/541662/), рассматриваем на пример ESP32. Обсудили почему нужно [правильно питать микроконтроллеры](https://habr.com/ru/post/146987/) и что такое блокировочный конденсатор.

{{< episode "ep-erb4ql/a-a4q2n6f" >}}
