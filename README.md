# 6N2P podcast hugo website
Avaliable here [6n2p.gitlab.com](https::/6n2p.gitlab.com)

 > **How to start work with repo?**
```bash
git clone https://gitlab.com/6n2p/6n2p.gitlab.io.git
git submodule init
git submodule update
```

Then you can run `hugo server` and see result at [localhost:1313](http://localhost:1313)

Any questions? Try to message me (*@likipiki*)

